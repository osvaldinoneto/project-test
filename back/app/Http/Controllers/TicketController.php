<?php

namespace App\Http\Controllers;

// use JWTAuth;
use App\Ticket;
use Illuminate\Http\Request;
use Tymon\JWTAuth\JWTAuth;

class TicketController extends Controller
{
  /**
   * @var
   */
  protected $auth;
  protected $user;

  /**
   * TicketController constructor.
   */
  public function __construct(JWTAuth $auth)
  {
    $this->auth = $auth;
    $this->user = $this->auth->parseToken()->authenticate();
  }

  /**
   * @return mixed
   */
  public function index()
  {

    $tickets = $this->user->tickets()->get()->toArray();

    if ('1' == $this->user->type_id) {
      $tickets = Ticket::all();
    }

    return response()->json([
      'data'    => $tickets,
      'success' => true,
      'token'   => (string) $this->auth->getToken(),
    ], 200);
  }

  /**
   * @param $id
   * @return \Illuminate\Http\JsonResponse
   */
  public function show($id)
  {

    if ('1' == $this->user->type_id) {
      $ticket = Ticket::find($id);
    } else {
      $ticket = $this->user->tickets()->find($id);
    }

    if (!$ticket) {
      return response()->json([
        'success' => false,
        'message' => 'Sorry, ticket with id ' . $id . ' cannot be found.',
      ], 400);
    }

    return response()->json([
      'data'    => $ticket,
      'success' => true,
      'token'   => (string) $this->auth->getToken(),
    ], 200);
  }

  /**
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   * @throws \Illuminate\Validation\ValidationException
   */
  public function store(Request $request)
  {
    $this->validate($request, [
      'title'       => 'required',
      'description' => 'required',
    ]);

    $ticket              = new Ticket();
    $ticket->title       = $request->title;
    $ticket->description = $request->description;

    if ($this->user->tickets()->save($ticket)) {
      return response()->json([
        'data'    => $ticket,
        'success' => true,
        'token'   => (string) $this->auth->getToken(),
      ], 200);
    } else {
      return response()->json([
        'success' => false,
        'message' => 'Sorry, ticket could not be added.',
      ], 500);
    }
  }

  /**
   * @param Request $request
   * @param $id
   * @return \Illuminate\Http\JsonResponse
   */
  public function update(Request $request, $id)
  {

    if ('1' == $this->user->type_id) {
      $ticket = Ticket::find($id);
    } else {
      $ticket = $this->user->tickets()->find($id);
    }

    if (!$ticket) {
      return response()->json([
        'success' => false,
        'message' => 'Sorry, ticket with id ' . $id . ' cannot be found.',
      ], 400);
    }

    $updated = $ticket->fill($request->all())->save();

    if ($updated) {
      return response()->json([
        'ticket'  => $ticket,
        'success' => true,
        'token'   => (string) $this->auth->getToken(),
      ], 200);
    } else {
      return response()->json([
        'success' => false,
        'message' => 'Sorry, ticket could not be updated.',
      ], 500);
    }
  }

  /**
   * @param $id
   * @return \Illuminate\Http\JsonResponse
   */
  public function destroy($id)
  {
    $ticket = $this->user->tickets()->find($id);

    if ('1' == $this->user->type_id) {
      $ticket = Ticket::find($id);
    }

    if (!$ticket) {
      return response()->json([
        'success' => false,
        'message' => 'Sorry, ticket with id ' . $id . ' cannot be found.',
      ], 400);
    }

    if ($ticket->delete()) {
      return response()->json([
        'success' => true,
        'token'   => (string) $this->auth->getToken(),
      ], 204);
    } else {
      return response()->json([
        'success' => false,
        'message' => 'Ticket could not be deleted.',
      ], 500);
    }
  }

  public function requestTicket($id)
  {
    $ticket = Ticket::find($id);

    if (!$ticket) {
      return response()->json([
        'success' => false,
        'message' => 'Sorry, ticket with id ' . $id . ' cannot be found.',
      ], 400);
    }

    $ticket->ticket_order = '1';
    $updated              = $ticket->save();

    if ($updated) {
      return response()->json([
        'ticket'  => $ticket,
        'success' => true,
        'token'   => (string) $this->auth->getToken(),
      ], 200);
    } else {
      return response()->json([
        'success' => false,
        'message' => 'Sorry, ticket could not be requested.',
      ], 500);
    }
  }

  public function acceptTicket($id)
  {
    $ticket = Ticket::find($id);

    if (!$ticket) {
      return response()->json([
        'success' => false,
        'message' => 'Sorry, ticket with id ' . $id . ' cannot be found.',
      ], 400);
    }

    $ticket->ticket_order = '2';
    $updated              = $ticket->save();

    if ($updated) {
      return response()->json([
        'ticket'  => $ticket,
        'success' => true,
        'token'   => (string) $this->auth->getToken(),
      ], 200);
    } else {
      return response()->json([
        'success' => false,
        'message' => 'Sorry, ticket could not be requested.',
      ], 500);
    }
  }
}
