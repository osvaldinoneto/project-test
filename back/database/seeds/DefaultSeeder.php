<?php

use Illuminate\Database\Seeder;

class DefaultSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$roleAdmin = \App\Type::create([
    		'name'    => 'Administrador'
    	]);

    	$roleUser = \App\Type::create([
    		'name'    => 'Usuario'
    	]);

    	$admin = \App\User::create([
    		'name'           => 'Administrador',
    		'email'          => 'admin@mail.com',
    		'password'       => bcrypt('123123'),
    		'type_id'		 => $roleAdmin->id,
    	]);

    	$user = \App\User::create([
    		'name'           => 'Usuario',
    		'email'          => 'user@mail.com',
    		'password'       => bcrypt('123123'),
    		'type_id'		 => $roleUser->id,
    	]);

    	$faker = Faker\Factory::create('pt_BR');
    	for($i=1; $i <= 10; $i++) {
    		$roleUser = \App\Ticket::create([
    			'user_id'    => $faker->numberBetween(1, 2),
                'title'      => $faker->word(),
                'description'=> $faker->text(),
    		]);
    	}
    }
}
