<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
  return $request->user();
});

Route::post('login', 'APIController@login');
Route::post('register', 'APIController@register');

Route::group(['middleware' => 'auth.jwt'], function () {

  Route::get('logout', 'APIController@logout');
  Route::get('users', 'APIController@users');

  Route::get('tickets', 'TicketController@index');
  Route::get('tickets/{id}', 'TicketController@show');
  Route::post('tickets', 'TicketController@store');
  Route::put('tickets/{id}', 'TicketController@update');
  Route::delete('tickets/{id}', 'TicketController@destroy');

  Route::get('request-ticket/{id}', 'TicketController@requestTicket');
  Route::get('accept-ticket/{id}', 'TicketController@acceptTicket');
});
