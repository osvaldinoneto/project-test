# Frontend

## :computer: Run it!

```bash

# Install the dependencies
yarn install

# Start the project
yarn start

```
---

# Backend

```bash

# Install the dependencies
composer install

cp .env.example .env

# Setting Database
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE='your database name'
DB_USERNAME='your username database'
DB_PASSWORD='your password database'

# Create Tables
php artisan migrate

# Seed Tables
php artisan db:seed

# Generate token jwt
php artisan jwt:secret

# Clear cache configs
php artisan config:clear

# Start Server to test
php artisan serve

```

# Users

```bash
# Administrator
Login: admin@mail.com
Password: 123123

# Usuario
Login: user@mail.com
Password: 123123
```

Made with much :purple_heart: and :muscle: by Osvaldino Neto :blush: <a href="https://www.linkedin.com/in/osvaldinoneto/">Talk to me!</a>