# Frontend

## :computer: Run it!

```bash

# Install the dependencies
yarn install

# Start the project
yarn start

```
---

Made with much :purple_heart: and :muscle: by Osvaldino Neto :blush: <a href="https://www.linkedin.com/in/osvaldinoneto/">Talk to me!</a>