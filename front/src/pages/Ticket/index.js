import React, { useState, useEffect } from 'react';
import ReactLoading from 'react-loading';
import { Link, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import {
  MdModeEdit,
  MdDeleteForever,
  MdAdd,
  MdCheckBox,
  MdAccessTime
} from 'react-icons/md';
import { useSelector } from 'react-redux';
import api from '~/services/api';
import history from '~/services/history';

import { Container, TicketTitle, TicketInfo } from './styles';

export default function Ticket() {
  const [loading, setLoading] = useState(false);

  const type = useSelector(state => state.user.profile.type_id);

  const { ticket_id } = useParams();

  const [ticket, setTicket] = useState({});

  useEffect(() => {
    async function loadTicket() {
      const response = await api.get(`tickets/${ticket_id}`);
      setTicket(response.data.data);
    }

    loadTicket();
  }, [ticket_id]);

  async function handleCancelTicket(id) {
    try {
      setLoading(true);
      await api.delete(`tickets/${id}`);
      toast.success('Ticket canceled!');
      setLoading(false);
      history.push('/dashboard');
    } catch (error) {
      setLoading(false);
      toast.error('Ops, something went wrong!');
    }
  }

  async function handleRequestTicket(id) {
    try {
      setLoading(true);
      await api.get(`request-ticket/${id}`);
      toast.success('Ticket Requested!');
      setLoading(false);
      history.push('/dashboard');
    } catch (error) {
      setLoading(false);
      toast.error('Ops, something went wrong!');
    }
  }

  async function handleAcceptTicket(id) {
    try {
      setLoading(true);
      await api.get(`accept-ticket/${id}`);
      toast.success('Ticket Accepted!');
      setLoading(false);
      history.push('/dashboard');
    } catch (error) {
      setLoading(false);
      toast.error('Ops, something went wrong!');
    }
  }

  function renderButton(param) {
    switch (param) {
      case '1':
        return (
          <div>
            <button
              type="button"
              style={{ background: '#ffc800', color: '#22202c' }}
            >
              <MdAccessTime />
              {loading ? (
                <ReactLoading
                  type="spin"
                  color="#fff"
                  height="18px"
                  width="18px"
                />
              ) : (
                'Requested ticket'
              )}
            </button>
          </div>
        );
      case '2':
        return (
          <div>
            <button type="button" style={{ background: '#1d9010' }}>
              <MdCheckBox />
              {loading ? (
                <ReactLoading
                  type="spin"
                  color="#fff"
                  height="18px"
                  width="18px"
                />
              ) : (
                'Accepted ticket'
              )}
            </button>
          </div>
        );
      default:
        return (
          <div>
            <button
              type="button"
              style={{ background: '#304db7' }}
              onClick={() => handleRequestTicket(ticket.id)}
            >
              <MdAdd />
              {loading ? (
                <ReactLoading
                  type="spin"
                  color="#fff"
                  height="18px"
                  width="18px"
                />
              ) : (
                'Request ticket'
              )}
            </button>
          </div>
        );
    }
  }

  return (
    <Container>
      <TicketTitle>
        <h1>{ticket.title}</h1>
        {type == '1' && (
          <div>
            {ticket.ticket_order === '1' && (
              <button
                type="button"
                style={{ background: '#304db7' }}
                onClick={() => handleAcceptTicket(ticket.id)}
              >
                <MdAdd />
                Accept ticket
              </button>
            )}
            <Link
              to={`/ticket_editor/${ticket_id}`}
              style={{ background: 'blue' }}
            >
              <MdModeEdit />
              Edit ticket
            </Link>
            <button
              type="button"
              style={{ background: 'red' }}
              onClick={() => handleCancelTicket(ticket.id)}
            >
              <MdDeleteForever />
              {loading ? (
                <ReactLoading
                  type="spin"
                  color="#fff"
                  height="18px"
                  width="18px"
                />
              ) : (
                'Remove ticket'
              )}
            </button>
          </div>
        )}
        {type === '2' && <div>{renderButton(ticket.ticket_order)}</div>}
      </TicketTitle>

      <TicketInfo>
        <p>{ticket.description}</p>
      </TicketInfo>
    </Container>
  );
}
