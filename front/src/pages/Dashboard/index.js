import React, { useState, useEffect } from 'react';
import { toast } from 'react-toastify';
import { Link } from 'react-router-dom';
import {
  MdAddCircleOutline,
  MdAdd,
  MdCheckBox,
  MdAccessTime
} from 'react-icons/md';
import { useSelector } from 'react-redux';
import history from '~/services/history';
import api from '~/services/api';

import { Container, Tickets, Ticket } from './styles';

export default function Dashboard() {
  const [tickets, setTickets] = useState([]);

  const type = useSelector(state => state.user.profile.type_id);  

  useEffect(() => {
    async function loadTickets() {
      const response = await api.get('tickets');

      try {
        setTickets(response.data.data);
      } catch (err) {
        toast.error(response.data.message);
      }
    }

    loadTickets();
  }, []);

  function handleTicketSelection(id) {
    history.push(`ticket/${id}`);
  }

  function renderLegend() {
    return (
      <>
      <button type="button" style={{ background: '#ffc800' }}>
            Requested
          </button>
          <button type="button" style={{ background: '#1d9010' }}>
            Accepted
          </button>
          <button type="button" style={{ background: '#304db7' }}>
            To Request
          </button>
          </>
    );
  }
  
  function renderNewTicket() {
    return (
      <>
      <Link to="/ticket_creator" style={{ background: 'red' }}>
            <MdAddCircleOutline />
            New Ticket
          </Link>
      </>
    );
  }

  function renderButton(param) {
    switch (param) {
      case '1':
        return (
          <div>
            <button type="button" style={{ background: '#ffc800' }}>
              <MdAccessTime />
            </button>
          </div>
        );
      case '2':
        return (
          <div>
            <button type="button" style={{ background: '#1d9010' }}>
              <MdCheckBox />
            </button>
          </div>
        );
      default:
        return (
          <div>
            <button type="button" style={{ background: '#304db7' }}>
              <MdAdd />
            </button>
          </div>
        );
    }
  }

  return (
    <Container>
      <div>
        <h1>My Tickets</h1>
        <div>{renderLegend()}</div>
        {type == '1' && (
          <div>{renderNewTicket()}</div>
        )}
      </div>

      <Tickets>
        {tickets.length > 0 ? (
          tickets.map(ticket => (
            <Ticket
              key={ticket.id}
              onClick={() => handleTicketSelection(ticket.id)}
            >
              <div>
                <strong>{ticket.title}</strong>
              </div>
              <span>{ticket.description}</span>
              <div>{renderButton(ticket.ticket_order)}</div>
            </Ticket>
          ))
        ) : (
          <h1>You do not have any tickets</h1>
        )}
      </Tickets>
    </Container>
  );
}
