import React, { useState, useEffect } from 'react';
import ReactLoading from 'react-loading';
import { toast } from 'react-toastify';
import { Form, Input } from '@rocketseat/unform';

import { useSelector } from 'react-redux';
import { Container } from './styles';
import api from '~/services/api';
import history from '~/services/history';

export default function TicketCreator() {
  const type = useSelector(state => state.user.profile.type_id);
  if (type != '1') {
    history.push('/dashboard');
  }

  const [loading, setLoading] = useState(false);

  const [users, setUsers] = useState([]);
  useEffect(() => {
    async function loadUsers() {
      const response = await api.get('users');
      try {
        setUsers(response.data.users);
      } catch (err) {
        toast.error(response.data.message);
      }
    }

    loadUsers();
  }, []);

  async function handleSubmit(data) {
    try {
      setLoading(true);
      await api.post('tickets', data);
      toast.success('Ticket created successfully!');
      setLoading(false);
      history.push('/dashboard');
    } catch (error) {
      setLoading(false);
      toast.error('Ops, something went wrong!');
    }
  }

  return (
    <Container>
      <Form onSubmit={handleSubmit}>
        <Input name="title" type="text" placeholder="Title" />
        <Input
          multiline
          rows="5"
          name="description"
          placeholder="Description"
        />
        {users.length > 0 ? (
          <select>
            <option key="0" value="">
              Select User
            </option>
            {users.map(user => (
              <option key={user.id} value="user.id">
                {user.name}
              </option>
            ))}
          </select>
        ) : (
          <h1>You do not have any tickets</h1>
        )}
        <button type="submit">
          {loading ? (
            <ReactLoading type="spin" color="#fff" height="32px" width="32px" />
          ) : (
            'Create ticket'
          )}
        </button>
      </Form>
    </Container>
  );
}
