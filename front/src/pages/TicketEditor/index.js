import React, { useState, useEffect } from 'react';
import ReactLoading from 'react-loading';
import { toast } from 'react-toastify';
import { Form, Input } from '@rocketseat/unform';
import { useParams } from 'react-router-dom';

import { useSelector } from 'react-redux';
import { Container } from './styles';

import api from '~/services/api';
import history from '~/services/history';

export default function TicketEditor() {
  const type = useSelector(state => state.user.profile.type_id);
  if (type != '1') {
    history.push('/dashboard');
  }

  const [users, setUsers] = useState([]);
  useEffect(() => {
    async function loadUsers() {
      const response = await api.get('users');
      try {
        setUsers(response.data.users);
      } catch (err) {
        toast.error(response.data.message);
      }
    }

    loadUsers();
  }, []);

  const [updateLoading, setUpdateLoading] = useState(false);
  const [loading, setLoading] = useState(true);

  const { ticket_id } = useParams();
  const [ticket, setTicket] = useState();
  const [userId, setUserId] = useState();

  useEffect(() => {
    async function loadTicket() {
      const response = await api.get(`tickets/${ticket_id}`);
      setTicket(response.data.data);
      setUserId(response.data.data.user_id);
      setLoading(false);
    }

    loadTicket();
  }, [ticket_id]);

  async function handleSubmit(data) {
    try {
      setUpdateLoading(true);
      const fullData = { ...data, id: ticket_id, user_id: userId };
      await api.put(`tickets/${ticket_id}`, fullData);
      toast.success('Ticket updated successfully!');
      setUpdateLoading(false);
      history.push('/dashboard');
    } catch (error) {
      setUpdateLoading(false);
      toast.error('Ops, something went wrong!');
    }
  }

  function handleSelectChange(event) {
    setUserId(event.target.value);
  }

  return (
    <Container>
      {loading ? (
        <ReactLoading type="spin" color="#fff" height="64px" width="64px" />
      ) : (
        <Form initialData={ticket} onSubmit={handleSubmit}>
          <Input name="title" type="text" placeholder="Title" />
          <Input
            multiline
            rows="5"
            name="description"
            type="text"
            placeholder="Description"
          />
          <select name="user_id" value={userId} onChange={handleSelectChange}>
            <option>Select User</option>
            {users.map(user => (
              <option key={user.id} value={user.id}>
                {user.name}
              </option>
            ))}
          </select>
          <button type="submit">
            {updateLoading ? (
              <ReactLoading
                type="spin"
                color="#fff"
                height="128px"
                width="12px"
              />
            ) : (
              'Update ticket'
            )}
          </button>
        </Form>
      )}
    </Container>
  );
}
