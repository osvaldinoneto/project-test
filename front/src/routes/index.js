import React from 'react';
import { Switch } from 'react-router-dom';
import Route from './Route';

import SignIn from '~/pages/SignIn';
import Dashboard from '~/pages/Dashboard';
import Ticket from '~/pages/Ticket';
import TicketCreator from '~/pages/TicketCreator';
import TicketEditor from '~/pages/TicketEditor';
import NotFound from '~/components/NotFound';

export default function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={SignIn} />

      <Route path="/dashboard" component={Dashboard} isPrivate />

      <Route path="/ticket/:ticket_id" component={Ticket} isPrivate />
      <Route path="/ticket_creator" component={TicketCreator} isPrivate />
      <Route
        path="/ticket_editor/:ticket_id"
        component={TicketEditor}
        isPrivate
      />

      <Route path="/" component={NotFound} />
    </Switch>
  );
}
